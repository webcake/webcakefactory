const path = require('path');

module.exports = {
  siteTitle: `Webcake Factory`,
  siteTitleShort: `Webcake`,
  siteDescription: `Webcake Factory Digital Agency. We bake digital experiences.`,
  siteUrl: `TODO`,
  themeColor: `#000`,
  backgroundColor: `#fff`,
  pathPrefix: null,
  logo: path.resolve(__dirname, 'src/images/icon.png'),
  social: {
    twitter: `TODO`,
    fbAppId: `TODO`,
  },
};
