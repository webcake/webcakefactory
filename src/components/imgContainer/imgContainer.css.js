import styled from 'styled-components';
import mask from '../../../content/pasaulisss.svg';

// This is the only way I got the svg file to be used by the mask-image property.
export const Container = styled.div`
  mask: url(${mask});
  mask-size: contain;
  margin-top: 10rem;
  width: 65%;
`;
