import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './imgContainer.css';

const ImgContainer = ({ children }) => <Container>{children}</Container>;

ImgContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ImgContainer;
