import React from 'react';
import PropTypes from 'prop-types';
import Box from 'components/box';
import ImgContainer from 'components/imgContainer';
import Bubble from 'components/bubble';
import Title from 'components/title';
import Img from 'gatsby-image';
//import { Box } from '../components/box.css';

const Hero = ({ homeJson }) => (
  <Box>
    <Title as="h1" size="xlarge">
      {homeJson.content.childMarkdownRemark.rawMarkdownBody}
    </Title>
    <ImgContainer>
      <Img
        fluid={
          homeJson.worldImage ? homeJson.worldImage.childImageSharp.fluid : {}
        }
        alt="World"
      />
    </ImgContainer>
    <Bubble />
  </Box>
);

Hero.propTypes = {
  homeJson: PropTypes.object.isRequired,
};

export default Hero;
