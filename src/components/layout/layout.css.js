import styled from 'styled-components';

export const FlexContainer = styled.div`
  display: flex;
`;

export const ContentWrapper = styled.div`
  display: block;
  width: 100%;
`;
