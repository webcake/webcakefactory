import styled from 'styled-components';
import MEDIA from 'helpers/mediaTemplates';

export const Text = styled.span`
  display: block;
  width: 35%;
  text-shadow: #03fbff 2px 1px;
  font-weight: ${({ size }) => () => {
    switch (size) {
      case 'xlarge':
        return '700';
      case 'large':
        return '400';
      default:
        return '500';
    }
  }};
  font-size: ${({ size }) => () => {
    switch (size) {
      case 'xlarge':
        return '7rem';
      case 'large':
        return '3.2rem';
      default:
        return '2rem';
    }
  }};
  line-height: 1.2;

  ${MEDIA.TABLET`
    font-size: ${({ size }) => () => {
      switch (size) {
        case 'xlarge':
          return '4rem';
        case 'large':
          return '2.6rem';
        default:
          return '2rem';
      }
    }};
  `};
`;
