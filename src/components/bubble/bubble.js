import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './bubble.css';

const Bubble = ({ children }) => <Container>{children}</Container>;

Bubble.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Bubble;
