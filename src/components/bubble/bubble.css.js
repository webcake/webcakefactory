import styled from 'styled-components';

export const Container = styled.div`
  width: 45rem;
  height: 45rem;
  background: #fff;
  margin-top: -7.5rem;
  border-radius: 50%;
`;
