import styled from 'styled-components';

export const Container = styled.nav`
  position: fixed;
  display: flex;
  width: 100vh;
  height: 7rem;
  justify-content: center;
  transform-origin: left top;
  transform: rotate(-90deg) translateX(-100%);
  ul {
    display: flex;
    list-style: none;
    padding: 0;
    align-items: center;

    li {
      text-transform: uppercase;
      font-size: 2rem;

      & + li {
        margin-left: 2rem;
      }
    }
  }
`;
