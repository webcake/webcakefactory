import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Container } from './nav.css';

const Nav = () => (
  <Container>
    <ul>
      <li>
        <Link to="/about">About</Link>
      </li>
      <li>
        <a href="https://github.com/fabe/gatsby-universal">GitHub</a>
      </li>
      <li>
        <Link to="/">Home</Link>
      </li>
    </ul>
  </Container>
);

Nav.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Nav;
