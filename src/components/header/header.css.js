import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  width: 7rem;
  height: 100%;
  background: linear-gradient(#096a70, #43173a);

  a {
    color: #fefefe;
    transition: color 0.2s ease;
    text-decoration: none;

    &:hover {
      color: darkgray;
    }
  }
`;
